import React, { Component } from "react";

class GoogleMaps extends Component {
  render() {

    return (
      <div style={{width : "100%" , marginTop : "100px"}}>


              <iframe
                src="https://maps.google.com/maps?q=10%20Rue%20de%20Penthi%C3%A8vre,%2075008%20Paris,%20France&t=&z=13&ie=UTF8&iwloc=&output=embed"
                frameborder="0"
                scrolling="yes"
                marginheight="0"
                marginwidth="0"
                width="100%"
                height="400px"
              ></iframe>

      </div>
    );
  }
}

export default GoogleMaps;


