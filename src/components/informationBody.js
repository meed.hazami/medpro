import React from 'react'
import Avatar from "../assets/Avatar.jpg";

export default function InformationBody() {
  return (
    <div>
      <div id="Rsocial"  className="title-inf">
        <h2 className="title-inf-contant">Plus d'information</h2>
      </div>
      <section>

        {/* box1 */}

        <div  className="flex-box-para">
          <img
            style={{ width: "300px", heigth: "auto" }}
            src={Avatar}
            alt="React Logo"
          />
          <div className="ajust-para">
            <h1 className="title-information">Réseau social</h1>
            <p className="inf-para-contant">
              Vous êtes un utilisateur accro aux platesformes sociales, sur
              SoWaySo vous recevez 30 centimes d’euro sur chaque texto, photo,
              vidéo, et même chaque publicité consultée sur notre plate-forme,
              et vos gains mensuels peuvent aller jusqu’à 180€ et des bons
              d’achat Interne jusqu’à 630 €
            </p>
          </div>
        </div>
        <span id="solidaire" ></span>
        {/* box2 */}

        <div  className="flex-box-para">
        <div className="ajust-para">
        <h1 className="title-information">Univers solidaire </h1>
        <p className="inf-para-contant">
          Vous êtes vidéo blogueur, producteur médiatique, artiste, acteur,
          chanteur, et même entreprise de diffusion médiatique et presse.
          SoWaySo vous offre le meilleur support de diffusion Web digitale pour
          pouvoir partager vos productions et recevoir des gains jusqu’à 10 fois
          plus que toutes autres plates-formes et surtout pour fidéliser vos
          fans par des cadeaux de prestige. Jusqu’à 10 000 € pour un million de
          vu
        </p>
        </div>
        <img
            style={{ width: "300px", heigth: "auto" }}
            src={Avatar}
            alt="React Logo"
          />
        </div>
        <span id="plateformes" ></span>
        
        {/* box3 */}

        <div  className="flex-box-para">
          <img
            style={{ width: "300px", heigth: "auto" }}
            src={Avatar}
            alt="React Logo"
          />
          <div className="ajust-para">
        <h1 className="title-information">Gagner de l'argent</h1>
        <p>
          Vous aimez partager votre journée et activités avec vos amis par des
          textos et des photos quotidiennement. SoWaySo vous offre l’opportunité
          de recevoir des primes équivalents à 05 € sur chaque 100 vu sur chaque
          publication personnelle.
        </p>
        </div>
        </div>
        <span id="TanitCoin"></span>
         {/* box4 */}

         <div className="flex-box-para">
        <div className="ajust-para">
        <h1 className="title-information">Valorisation des activités</h1>
        <p className="inf-para-contant">
          SoWaySo offre à chaque annonceur SoCase et/ou SoMark l’opportunité de
          diffuser leur produit et service solidairement avec leurs clientèles
          et la possibilité de recevoir des certificats de garantie de chiffre
          d’affaire sur notre plate-forme. Jusqu’à 1 000 0000 € pour 15 millions
          de fans
          </p>
        </div>
        <img
            style={{ width: "300px", heigth: "auto" }}
            src={Avatar}
            alt="React Logo"
          />
        </div>
      </section>
    </div>
  )
}
