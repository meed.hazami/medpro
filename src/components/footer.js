import React, { Component } from "react";
import FacebookIcon from "@material-ui/icons/Facebook";
import AddIcCallIcon from "@material-ui/icons/AddIcCall";
import InstagramIcon from "@material-ui/icons/Instagram";
import EmailIcon from "@material-ui/icons/Email";
import logo from "../assets/LogoSowaysoLab.png";

export default class Footer extends Component {
  render() {
    return (
      <section
        style={{
          backgroundColor: "#111111",
          width: "100%",
          minHeight: "400px",
        }}
      >
        <div
          style={{
            display: "flex",
            justifyContent: "space-around",
            alignItems: "center",
            width: "100%",
            flexWrap: "wrap",
            minHeight: "300px",
          }}
        >
          <section
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <img src={logo} style={{ width: "150px", height: "auto" }} />
          </section>

          <div style={{ height: "20px", height: "100px", marginTop: "50px" }}>
            <h3
              style={{
                color: "#409F8E",
              }}
            >
              Contactez-nous :
            </h3>
            <ul style={{ textAlign: "start", listStyleType: "none" }}>
              <li style={{ paddingLeft: "50px" }}>
                <a
                  style={{ color: "#409F8E", textDecoration: "none" }}
                  href="tel:00 33 7 50 51 19 55"
                >
                  <AddIcCallIcon /> 00 33 7 50 51 19 55
                </a>
              </li>

              <li style={{ color: "#409F8E", paddingLeft: "50px" }}>
                <a
                  style={{ color: "#409F8E", textDecoration: "none" }}
                  href="mailto:Support.ut@sowayso.com"
                >
                  <EmailIcon /> Support.ut@sowayso.com
                </a>
              </li>
            </ul>
          </div>

          <div style={{ height: "20px", height: "100px", marginTop: "50px" }}>
            <h3 style={{ color: "#409F8E" }}>Réseaux sociaux :</h3>
            <ul style={{ textAlign: "start", listStyleType: "none" }}>
              <li style={{ color: "#fff" }}>
                <a
                  style={{ color: "#409F8E", textDecoration: "none" }}
                  target="_blank"
                  href="https://www.facebook.com/"
                >
                  <FacebookIcon /> facebook
                </a>
              </li>
              <li style={{ color: "#409F8E" }}>
                <a
                  style={{ color: "#409F8E", textDecoration: "none" }}
                  target="_blank"
                  href="https://www.instagram.com/"
                >
                  <InstagramIcon /> Instagram
                </a>
              </li>
            </ul>
          </div>
        </div>
        <h8 style={{ color: "#fff" }} id="CONTACT">
          PlATFORME WEB RÉALISÉ PAR
          <h8 style={{ color: "#409F8E" }}> SOWAYSOLAB</h8>| © 2022. TOUS DROITS
          RÉSERVÉS
        </h8>
      </section>
    );
  }
}
