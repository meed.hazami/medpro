import React from 'react'
import Carousel from "./carousel"
import Information from "./information"
import "./style.css"

export default function Index() {
  return (
    <div className='flex-car-info'>
       <Carousel/>
       <Information/>
    </div>
  )
}
