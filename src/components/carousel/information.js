import React from 'react'

export default function Information() {
  return (
    <div>
        <ul className='info-para'>
            <li style={{color : "#419F8E"}}>
                <a className='info-element' href='#Rsocial'>C’est plus qu’un réseau social, c’est plus qu’un crypto monnaie d’investissement</a>
            </li>
            <li style={{color : "#419F8E"}}>
                <a className='info-element' href='#solidaire'>C’est notre univers solidaire pour un monde meilleur</a>
            </li>
            <li style={{color : "#419F8E"}}>
                <a className='info-element' href='#plateformes'>Sur les autres plateformes je passe beaucoup de temps sans aucun gain</a>
            </li>
            <li style={{color : "#419F8E"}}>
                <a className='info-element' href='#TanitCoin'>SoWaySo et TanitCoin valorise tous mes activités avec des primes de qualité</a>
            </li>
        </ul>
    </div>
  )
}
