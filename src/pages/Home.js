import React from "react";
import { useSelector } from "react-redux";
import Spinner from "../components/Spinner";
import CarouselIndex from "../components/carousel/index";
import InformationBody from "../components/informationBody"
import GoogleMaps from "../components/googleMaps"
import Footer from "../components/footer"

const Home = () => {
  const { loading } = useSelector((state) => ({ ...state.tour }));
  if (loading) {
    return <Spinner />;
  }
  return (
    <div style={{ marginTop: "100px" }}>
      <CarouselIndex />
      <InformationBody/>
      <GoogleMaps/>
      <Footer/>
    </div>
  );
};

export default Home;
